package cn.clouddemo.service;

import cn.clouddemo.entity.ProductComment;

import java.util.List;

/**
 * Created by shenzx on 2019/2/16.
 */
public interface ProductCommentService {

    /**
     * 加载指定商品的评论列表
     * @param productId
     * @return
     */
    List<ProductComment> findAllByProduct(Long productId);

}
