package cn.clouddemo.service.impl;

import cn.clouddemo.dao.ProductCommentDao;
import cn.clouddemo.entity.ProductComment;
import cn.clouddemo.service.ProductCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by shenzx on 2019/2/16.
 */
@Service("productCommentService")
public class ProductCommentServiceImpl implements ProductCommentService {

    @Autowired
    private ProductCommentDao productCommentRepository;

    @Override
    public List<ProductComment> findAllByProduct(Long productId) {
        return this.productCommentRepository.findByProductIdOrderByCreated(productId);
    }
}
