package cn.clouddemo.service;

import cn.clouddemo.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by shenzx on 2019/2/16.
 */
public interface ProductService {

    /**
     * 获取商品配置的分页数据
     * @param pageable 分页参数
     * @return 分页数据
     */
    Page<Product> getPage(Pageable pageable);

    /**
     * 加载指定的商品配置
     * @param id 商品配置ID
     * @return
     */
    Product load(Long id);

}
