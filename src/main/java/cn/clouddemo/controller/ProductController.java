package cn.clouddemo.controller;

import cn.clouddemo.dto.ProductCommentDto;
import cn.clouddemo.dto.ProductDto;
import cn.clouddemo.dto.UserDto;
import cn.clouddemo.entity.Product;
import cn.clouddemo.entity.ProductComment;
import cn.clouddemo.service.ProductCommentService;
import cn.clouddemo.service.ProductService;
import cn.clouddemo.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by shenzx on 2019/2/16.
 */
@RestController
@RequestMapping("/products")
public class ProductController {

        @Autowired
        private ProductService productService;

        @Autowired
        private UserService userService;

        @Autowired
        private ProductCommentService productCommentService;

        /**
         * 获取产品信息列表
         * @return
         */
        @RequestMapping(method = RequestMethod.GET)
        @ApiOperation(value = "获取商品分页数据", notes = "获取商品分页数据", httpMethod = "GET", tags = "商品管理相关Api")
        @ApiImplicitParams({
                @ApiImplicitParam(name = "page", value = "第几页，从0开始，默认为第0页", dataType = "int", paramType = "query"),
                @ApiImplicitParam(name = "size", value = "每一页记录数的大小，默认为20", dataType = "int", paramType = "query"),
                @ApiImplicitParam(name = "sort", value = "排序，格式为:property,property(,ASC|DESC)的方式组织，如sort=firstname&sort=lastname,desc", dataType = "String", paramType = "query")
        })
        public List<ProductDto> list(Pageable pageable) {
            Page<Product> page = this.productService.getPage(pageable);
            if (null != page) {
                return page.getContent().stream().map((product) -> {
                    return new ProductDto(product);
                }).collect(Collectors.toList());
            }
            return Collections.EMPTY_LIST;
        }

        @RequestMapping(value = "/{id}", method = RequestMethod.GET)
        @ApiOperation(value = "获取商品详情数据", notes = "获取商品详情数据", httpMethod = "GET", tags = "商品管理相关Api")
        @ApiImplicitParams({
                @ApiImplicitParam(name = "id", value = "商品的主键", dataType = "int", paramType = "path")
        })
        public ProductDto detail(@PathVariable Long id){
            Product product = this.productService.load(id);
            if (null == product)
                return null;
            return new ProductDto(product);
        }

    @RequestMapping(value = "/{id}/comments", method = RequestMethod.GET)
    @ApiOperation(value = "获取商品的评论列表", notes = "获取商品的评论列表", httpMethod = "GET", tags = "商品评论管理相关Api")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "商品的主键", dataType = "int", paramType = "path")
    })
    public List<ProductCommentDto> comments(@PathVariable Long id){
        List<ProductComment> commentList = this.productCommentService.findAllByProduct(id);
        if (null == commentList || commentList.isEmpty())
            return Collections.emptyList();

        ProductDto productDto = new ProductDto(this.productService.load(id));
        return commentList.stream().map((comment) -> {
            ProductCommentDto dto = new ProductCommentDto(comment);
            dto.setProduct(productDto);
            dto.setAuthor(new UserDto(this.userService.load(comment.getAuthorId())));
            return dto;
        }).collect(Collectors.toList());
    }

}
