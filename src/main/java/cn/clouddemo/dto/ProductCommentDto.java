package cn.clouddemo.dto;

import cn.clouddemo.entity.ProductComment;
import com.google.common.base.MoreObjects;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by shenzx on 2019/2/16.
 */
@ApiModel(value = "商品评论信息")
public class ProductCommentDto implements Serializable {

    @ApiModelProperty(value="评论主键Id")
    private Long id;

    @ApiModelProperty(value="所属商品")
    private ProductDto product;

    @ApiModelProperty(value="评论作者")
    private UserDto author;

    @ApiModelProperty(value="评论内容")
    private String content;

    @ApiModelProperty(value="创建时间")
    private Date created;

    public ProductCommentDto() {

    }

    public ProductCommentDto(ProductComment productComment) {
        this.id = productComment.getId();
        this.content = productComment.getContent();
        this.created = productComment.getCreated();
    }

    @Override
    public String toString() {
        return this.toStringHelper().toString();
    }

    protected MoreObjects.ToStringHelper toStringHelper() {
        return MoreObjects.toStringHelper(this)
                .add("id", getId())
                .add("productId", getProduct());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductDto getProduct() {
        return product;
    }

    public void setProduct(ProductDto product) {
        this.product = product;
    }

    public UserDto getAuthor() {
        return author;
    }

    public void setAuthor(UserDto author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
